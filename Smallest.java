package bla;
public class Smallest extends Number{

	private int minim;
	public Smallest(){
		minim=99999999;
	}
	@Override
	public void property(int x)
	{
		if(x<minim)
			minim=x;
	}
	@Override
	public void view()
	{
		System.out.print("\nSmallest number: "+minim);
	}
}
