package bla;
import java.util.*;
public class Prime_nr extends Number {
	private Vector<Integer>v=new Vector<Integer>(10,2);
	public Prime_nr(){
		
	}
	@Override
	public void property(int x) {
		int i;
		for(i=2;i<=x/2;i++)
		{
			if(x%i==0)
				return;
		}
		v.add(x);
	}
	@Override
	public void view(){
		int n=v.size();
		if(n==0)
			{
				System.out.print("\nPrime numbers don't exist");
				return;
			}
		System.out.print("Prime numbers: ");		
		for(int i=0;i<n;i++)
			System.out.print(v.get(i) + " ");
		System.out.println();
	}
}
