package bla;
import java.util.*;
public class Palindromes extends Number {
	
	private Vector<Integer> val=new Vector<Integer>(10,2);
	public Palindromes(){
		
	}
	@Override
	public void property(int x) {
		int aux;
		int pal=0;
			aux=x;
			while(aux!=0)
			{
				pal=(aux%10)+pal*10;
				aux=aux/10;
			}
		
		if(pal==x)
			val.add(x);

		
	}
	@Override
	public void view(){
		int n=val.size();
		if(n==0)
			{
				System.out.print("\nPalindromes don't exist");
				return;
			}
		System.out.print("\nPalindromes: ");		
		for(int i=0;i<n;i++)
			System.out.print(val.get(i) + " ");
		System.out.println();
	}
}
