package bla;
import java.util.*;

public class Meniu{

private static Scanner scan;

public static void main(String[] args){
    scan = new Scanner(System.in);
    System.out.print("Enter integers please ");
    System.out.println("(EOF or non-integer to terminate): ");
    Number P= new Palindromes();
    Number A= new Prime_nr();
    Number M= new Biggest();
    Number m= new Smallest();
    int ok=0;
    int x;
    while(scan.hasNextInt()){
    	ok=1;
    	x= scan.nextInt();
         P.property(x);
         A.property(x);
         M.property(x);
         m.property(x);
    }
    if(ok==1)
    {
    	M.view();
    	m.view();
    	P.view();
    	A.view();
    }
    else
    {
    	System.out.print("\nEnd Of Program");
    }   
}

}
